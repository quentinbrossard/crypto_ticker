#!/usr/bin/env python
# -*- coding: utf-8 -*-import requests
from sys import argv
from typing import List
from requests import Request, Session, Timeout, TooManyRedirects
import multiprocessing
import time
import signal
import sys
import redis
import json
import logging

logging.basicConfig(level=logging.DEBUG)

crypto_list = ['BTC', 'ETH', 'BCH', 'XRP', 'USDT']

sense_hat_active = False
if sense_hat_active:
    from sense_hat import SenseHat
    logging.debug("Sense HAT is ACTIVE")
    sense = SenseHat()
    sense.show_message("CRYPTO TICKER BOOT...", text_colour=(0,0,200), back_colour=(0,0,0),scroll_speed=0.05)
else:
    logging.debug("Sense HAT deactivated")

class bcolors:
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'

def save_response(response):
    logging.debug("saving response for {} to redis".format(response['symbol']))
    logging.debug(str(response))
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    quote = response['quote']['CHF']
    value = {'price': quote['price'], 'percent_change_1h': quote['percent_change_1h'], 'percent_change_24h': quote['percent_change_24h'], 'last_updated': response['last_updated']}
    r.set("RATE_" + response['symbol'], json.dumps(value))

def read_prices():
    while True:
        logging.debug("reading prices from API")
        url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
        parameters = {
        'start':'1',
        'limit':'10',
        'convert':'CHF'
        }
        headers = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': '2facf473-a355-467a-b42b-744a959bd30e',
        }

        session = Session()
        session.headers.update(headers)

        try:
            response = session.get(url, params=parameters)
            data = response.json()
            for coin in data['data']:
                save_response(coin)
            logging.debug("sleeping...")
            time.sleep(60)

        except (ConnectionError, Timeout, TooManyRedirects) as e:
            print(e)


def format_price(currency):
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    cur_info_str = r.get("RATE_" + currency)
    if cur_info_str is None:
        return (currency + "/CHF : no prices available", 0.0, 0.0)
    else:
        cur_info = json.loads(cur_info_str)
        return (currency + "/CHF " + str(cur_info['price'])[:8], float(cur_info['percent_change_1h']), float(cur_info['percent_change_24h']))

def sense_display_prices():
    if sense_hat_active:
        sense = SenseHat()
    green = (0, 200, 0)
    red = (200, 0, 0)
    while True:
        print("=============================")
        for cur in crypto_list:
            price = format_price(cur)
            term_color = bcolors.GREEN if price[1] >= 0 else bcolors.RED
            pct_change = " (" + "{:+.2f}%".format(price[1]) + " 1H," + "{:+.2f}%".format(price[2]) + " 24H)"
            text_color = green if price[1] >= 0 else red
            if sense_hat_active:
                sense.show_message(price[0] + pct_change, text_colour=text_color, back_colour=(0,0,0),scroll_speed=0.05)
            else:
                print(term_color + price[0] + pct_change + bcolors.ENDC)

        time.sleep(5)

def clear_cache():
    logging.debug("deleting quotes for {}".format(crypto_list))
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    for coin in crypto_list:
        r.delete("RATE_" + coin)

def main():
    if len(sys.argv) == 2 and sys.argv[1] == 'clear':
        logging.debug("Clearing redis cache")
        clear_cache()
        exit(0)

    reader = multiprocessing.Process(target=read_prices)
    reader.start()

    writer = multiprocessing.Process(target=sense_display_prices)
    writer.start()
    time.sleep(30)

    def signal_handler(signal, frame):
            print('You pressed Ctrl+C!')
            reader.terminate()
            writer.terminate()
            if sense_hat_active:
                sense = SenseHat()
                sense.clear()
            sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    print('Press Ctrl+C')
    signal.pause()

if __name__ == "__main__":
    main()

